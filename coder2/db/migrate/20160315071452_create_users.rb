class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|

      t.string :username
      t.string :address
      t.string :city
      t.string :country
      t.string :phone
      t.string :about
      t.string :avatar
      t.string :website
      t.boolean :admin, default: false

      t.timestamps null: false
    end
  end
end

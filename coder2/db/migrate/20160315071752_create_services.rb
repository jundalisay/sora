class CreateServices < ActiveRecord::Migration
  def change
    create_table :services do |t|

    t.references :user, index: true, foreign_key: true
      
      t.string :service_name, null: false
      t.string :classification

      t.string :need
      t.integer :quantity
      t.string :measure

      t.string :cert
      t.integer :years_exp, default: 0
      t.string :pic
      t.text :description
      
      t.timestamps null: false
    end
  end
end

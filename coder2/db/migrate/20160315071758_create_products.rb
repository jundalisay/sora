class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|

    t.references :user, index: true, foreign_key: true

	 t.string :product_name
	 t.string :classification

	 t.string :need
	 t.integer :quantity
	 t.integer :min_order_q
	 t.string :measure

	 t.string :barcode
	 t.string :pic
	 t.text :description
      t.timestamps null: false
    end
  end
end
